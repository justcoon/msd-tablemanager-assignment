package msd.tablemanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * main application class
 *
 * @author pkotula
 */
@EntityScan
@ComponentScan
@EnableJpaRepositories
@EnableTransactionManagement
@EnableAutoConfiguration
@Configuration
public class Application {

    /**
     * column separator
     */
    public static final char TABLE_COLUMN_SEPARATOR = ',';

    /**
     * max value length
     */
    public static final int TABLE_VALUE_MAX_LENGTH = 2000;


    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
