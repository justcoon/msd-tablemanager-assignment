package msd.tablemanager.service;

import msd.tablemanager.Application;
import msd.tablemanager.domain.TableFile;
import msd.tablemanager.domain.TableFileRow;
import msd.tablemanager.repository.TableFileRepository;
import msd.tablemanager.repository.TableFileRowRepository;
import msd.tablemanager.repository.TableFileRowSpecification;
import msd.tablemanager.support.Utilities;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author pkotula
 */
@Service
@Transactional
public class TableFileServiceImpl implements TableFileService {


    @Autowired
    TableFileRepository tableFileRepository;
    @Autowired
    TableFileRowRepository tableFileRowRepository;

    @Override
    public TableFile createOrUpdateFile(String name, List<String[]> values) {
        Assert.notNull(name, "Name is required");
        Assert.notEmpty(values, "Values are required");

        TableFile file = getFile(name).orElse(new TableFile());

        file.setName(name);
        file.getHeader().clear();
        file.getHeader().addAll(Utilities.getValues(values.get(0), Application.TABLE_VALUE_MAX_LENGTH));

        List<TableFileRow> fileRows = new ArrayList<>();

        for (int i = 1; i < values.size(); i++) {
            TableFileRow fileRow = new TableFileRow();
            fileRow.setFile(file);
            fileRow.setRowIndex(i - 1);
            fileRow.setValues(Utilities.getValues(values.get(i), Application.TABLE_VALUE_MAX_LENGTH));
            fileRows.add(fileRow);
        }

        file.getRows().clear();
        file.getRows().addAll(fileRows);

        return tableFileRepository.save(file);
    }

    @Override
    public Optional<TableFile> getFile(String name) {
        Assert.notNull(name, "Name is required");

        List<TableFile> files = tableFileRepository.findByName(name);

        return files.isEmpty() ? Optional.empty() : Optional.of(files.get(0));
    }

    @Override
    public Optional<List<TableFileRow>> getFileRows(String name, Integer firstRowIndex, Integer lastRowIndex) {
        Assert.notNull(name, "Name is required");

        List<TableFile> files = tableFileRepository.findByName(name);

        if (files.isEmpty()) {
            return Optional.empty();
        }

        return Optional.of(tableFileRowRepository.findAll(new TableFileRowSpecification<>(name, firstRowIndex, lastRowIndex), new Sort(Sort.Direction.ASC, "rowIndex")));
    }
}
