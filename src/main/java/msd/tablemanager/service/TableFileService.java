package msd.tablemanager.service;

import msd.tablemanager.domain.TableFile;
import msd.tablemanager.domain.TableFileRow;

import java.util.List;
import java.util.Optional;

/**
 * The interface Table file service.
 *
 * @author pkotula
 */
public interface TableFileService {


    /**
     * Create or update table file.
     *
     * @param name   the name - required
     * @param values the values - required, not empty, first element is header
     * @return the table file
     */
    TableFile createOrUpdateFile(String name, List<String[]> values);

    /**
     * Gets file.
     *
     * @param name the name - required
     * @return the file
     */
    Optional<TableFile> getFile(String name);

    /**
     * Gets file rows.
     *
     * @param name          the name - required
     * @param firstRowIndex the first row index
     * @param lastRowIndex  the last row index
     * @return the file rows
     */
    Optional<List<TableFileRow>> getFileRows(String name, Integer firstRowIndex, Integer lastRowIndex);
}
