package msd.tablemanager.domain;

import msd.tablemanager.Application;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * Table file row.
 *
 * @author pkotula
 */
@Entity
@Table(name = "table_file_row", indexes = {@Index(name = "tfr_row_index_idx", columnList = "row_index")})
public class TableFileRow {
    /**
     * The Id.
     */
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    @Column(name = "id", nullable = false)
    Long id;

    /**
     * The File.
     */
    @NotNull
    @ManyToOne
    TableFile file;

    /**
     * The Row index.
     */
    @NotNull
    @Column(name = "row_index")
    Integer rowIndex;

    /**
     * The Values.
     */
    @ElementCollection
    @CollectionTable(name = "table_file_row_value", joinColumns = @JoinColumn(name = "table_file_row_id"))
    @Column(length = Application.TABLE_VALUE_MAX_LENGTH)
    List<String> values = new ArrayList<>();

    /**
     * Gets id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets file.
     *
     * @return the file
     */
    public TableFile getFile() {
        return file;
    }

    /**
     * Sets file.
     *
     * @param file the file
     */
    public void setFile(TableFile file) {
        this.file = file;
    }

    /**
     * Gets row index.
     *
     * @return the row index
     */
    public Integer getRowIndex() {
        return rowIndex;
    }

    /**
     * Sets row index.
     *
     * @param rowIndex the row index
     */
    public void setRowIndex(Integer rowIndex) {
        this.rowIndex = rowIndex;
    }

    /**
     * Gets values.
     *
     * @return the values
     */
    public List<String> getValues() {
        return values;
    }

    /**
     * Sets values.
     *
     * @param values the values
     */
    public void setValues(List<String> values) {
        this.values = values;
    }

    @Override
    public String toString() {
        return "TableFileRow{" +
                "id=" + id +
                ", file.id=" + (file != null ? file.getId() : null) +
                ", rowIndex=" + rowIndex +
                ", values=" + values +
                '}';
    }
}
