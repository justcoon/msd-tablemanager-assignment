package msd.tablemanager.domain;

import msd.tablemanager.Application;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * Table file entity
 *
 * @author pkotula
 */
@Entity
@Table(name = "table_file", indexes = {@Index(name = "tf_name_idx", columnList = "name")})
public class TableFile {
    /**
     * The Id.
     */
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    @Column(name = "id", nullable = false)
    Long id;

    /**
     * The Name.
     */
    @NotNull
    @Column(name = "name")
    String name;

    /**
     * The Header.
     */
    @ElementCollection
    @CollectionTable(name = "table_file_header", joinColumns = @JoinColumn(name = "table_file_id"))
    @Column(length = Application.TABLE_VALUE_MAX_LENGTH)
    List<String> header = new ArrayList<>();

    /**
     * The Rows.
     */
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "file")
    List<TableFileRow> rows = new ArrayList<>();

    /**
     * Gets id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets header.
     *
     * @return the header
     */
    public List<String> getHeader() {
        return header;
    }

    /**
     * Sets header.
     *
     * @param header the header
     */
    public void setHeader(List<String> header) {
        this.header = header;
    }

    /**
     * Gets rows.
     *
     * @return the rows
     */
    public List<TableFileRow> getRows() {
        return rows;
    }

    /**
     * Sets rows.
     *
     * @param rows the rows
     */
    public void setRows(List<TableFileRow> rows) {
        this.rows = rows;
    }

    @Override
    public String toString() {
        return "TableFile{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", header=" + header +
                '}';
    }
}
