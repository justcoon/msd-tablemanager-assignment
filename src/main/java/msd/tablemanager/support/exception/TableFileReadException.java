package msd.tablemanager.support.exception;

/**
 * Table file read exception.
 *
 * @author pkotula
 */
public class TableFileReadException extends Exception {

    public TableFileReadException(String message) {
        super(message);
    }

    public TableFileReadException(String message, Throwable cause) {
        super(message, cause);
    }

    public TableFileReadException(Throwable cause) {
        super(cause);
    }
}
