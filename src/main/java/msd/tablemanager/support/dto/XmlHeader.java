package msd.tablemanager.support.dto;

import org.springframework.util.Assert;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Collection;

/**
 * Xml header.
 *
 * @author pkotula
 */
@XmlRootElement(name = "header")
@XmlAccessorType(XmlAccessType.FIELD)
public class XmlHeader {

    @XmlElement(name = "value")
    private Collection<String> values;

    /**
     * Instantiates a new Xml header.
     *
     * @param values the values
     */
    public XmlHeader(Collection<String> values) {
        this.values = values;
    }

    /**
     * Instantiates a new Xml header.
     */
    public XmlHeader() {
    }

    /**
     * Gets values.
     *
     * @return the values
     */
    public Collection<String> getValues() {
        return values;
    }

    /**
     * Sets values.
     *
     * @param values the values
     */
    public void setValues(Collection<String> values) {
        this.values = values;
    }

    @Override
    public String toString() {
        return "XmlHeader{" +
                "values=" + values +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof XmlHeader)) return false;

        XmlHeader xmlHeader = (XmlHeader) o;

        return values != null ? values.equals(xmlHeader.values) : xmlHeader.values == null;
    }

    @Override
    public int hashCode() {
        return values != null ? values.hashCode() : 0;
    }

    /**
     * Instance of xml header.
     *
     * @param values the values
     * @return the xml header
     */
    public static XmlHeader instanceOf(Collection<String> values) {
        Assert.notNull(values, "Values are required");

        return new XmlHeader(values);
    }
}
