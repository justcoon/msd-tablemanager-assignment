package msd.tablemanager.support.dto;

import org.springframework.util.Assert;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Collection;

/**
 * Xml rows.
 *
 * @author pkotula
 */
@XmlRootElement(name = "rows")
@XmlAccessorType(XmlAccessType.FIELD)
public class XmlRows {

    @XmlElement(name = "row")
    private Collection<XmlRow> rows;

    /**
     * Instantiates a new Xml rows.
     *
     * @param rows the rows
     */
    public XmlRows(Collection<XmlRow> rows) {
        this.rows = rows;
    }

    /**
     * Instantiates a new Xml rows.
     */
    public XmlRows() {

    }

    /**
     * Gets rows.
     *
     * @return the rows
     */
    public Collection<XmlRow> getRows() {
        return rows;
    }

    /**
     * Sets rows.
     *
     * @param rows the rows
     */
    public void setRows(Collection<XmlRow> rows) {
        this.rows = rows;
    }

    @Override
    public String toString() {
        return "XmlRows{" +
                "rows=" + rows +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof XmlRows)) return false;

        XmlRows xmlRows = (XmlRows) o;

        return rows != null ? rows.equals(xmlRows.rows) : xmlRows.rows == null;
    }

    @Override
    public int hashCode() {
        return rows != null ? rows.hashCode() : 0;
    }

    /**
     * Instance of xml rows.
     *
     * @param rows the rows
     * @return the xml rows
     */
    public static XmlRows instanceOf(Collection<XmlRow> rows) {
        Assert.notNull(rows, "Rows are required");
        return new XmlRows(rows);
    }
}
