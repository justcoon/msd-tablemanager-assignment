package msd.tablemanager.support.dto;

import org.springframework.util.Assert;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Collection;

/**
 * Xml row.
 *
 * @author pkotula
 */
@XmlRootElement(name = "row")
@XmlAccessorType(XmlAccessType.FIELD)
public class XmlRow {

    @XmlElement(name = "value")
    private Collection<String> values;

    /**
     * Instantiates a new Xml row.
     *
     * @param values the values
     */
    public XmlRow(Collection<String> values) {
        this.values = values;
    }

    /**
     * Instantiates a new Xml row.
     */
    public XmlRow() {

    }

    /**
     * Gets values.
     *
     * @return the values
     */
    public Collection<String> getValues() {
        return values;
    }

    /**
     * Sets values.
     *
     * @param values the values
     */
    public void setValues(Collection<String> values) {
        this.values = values;
    }

    @Override
    public String toString() {
        return "XmlRow{" +
                "values=" + values +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof XmlRow)) return false;

        XmlRow xmlRow = (XmlRow) o;

        return values != null ? values.equals(xmlRow.values) : xmlRow.values == null;
    }

    @Override
    public int hashCode() {
        return values != null ? values.hashCode() : 0;
    }

    /**
     * Instance of xml row.
     *
     * @param values the values
     * @return the xml row
     */
    public static XmlRow instanceOf(Collection<String> values) {
        Assert.notNull(values, "Values are required");

        return new XmlRow(values);
    }
}
