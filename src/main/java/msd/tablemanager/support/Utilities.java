package msd.tablemanager.support;

import msd.tablemanager.domain.TableFileRow;
import msd.tablemanager.support.dto.XmlRow;
import msd.tablemanager.support.dto.XmlRows;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.Assert;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Utilities.
 *
 * @author pkotula
 */
public final class Utilities {

    private Utilities() {

    }

    /**
     * Gets values.
     *
     * @param values    the values
     * @param maxLength the max length
     * @return the values
     */
    public static List<String> getValues(String[] values, int maxLength) {
        Assert.notNull(values, "Values are required");
        return Arrays.stream(values).map(v -> StringUtils.left(v, maxLength)).collect(Collectors.toList());
    }

    /**
     * Gets xml rows.
     *
     * @param rows the rows
     * @return the xml rows
     */
    public static XmlRows getXmlRows(Collection<TableFileRow> rows) {
        Assert.notNull(rows, "Rows are required");
        return XmlRows.instanceOf(rows.stream().map(tr -> XmlRow.instanceOf(tr.getValues())).collect(Collectors.toList()));
    }
}
