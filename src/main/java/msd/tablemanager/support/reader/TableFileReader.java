package msd.tablemanager.support.reader;

import msd.tablemanager.support.exception.TableFileReadException;

import java.io.InputStream;
import java.util.List;

/**
 * Table file reader.
 *
 * @author pkotula
 */
public interface TableFileReader {

    /**
     * Read data list.
     *
     * @param inputStream the input stream
     * @return the list
     * @throws TableFileReadException the table file read exception
     */
    List<String[]> readData(InputStream inputStream, char separator) throws TableFileReadException;
}
