package msd.tablemanager.support.reader;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvParser;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import msd.tablemanager.support.exception.TableFileReadException;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.io.InputStream;
import java.util.List;

/**
 * @author pkotula
 */
@Component("csvTableFileReader")
public class CSVTableFileReader implements TableFileReader {

    @Override
    public List<String[]> readData(InputStream inputStream, char separator) throws TableFileReadException {
        Assert.notNull(inputStream, "Input stream required");

        try {
            CsvMapper mapper = new CsvMapper();
            CsvSchema bootstrapSchema = CsvSchema.emptySchema().withSkipFirstDataRow(false).withColumnSeparator(separator);
            mapper.enable(CsvParser.Feature.WRAP_AS_ARRAY);
            MappingIterator<String[]> readValues =
                    mapper.readerFor(String[].class).with(bootstrapSchema).readValues(inputStream);
            return readValues.readAll();
        } catch (Exception e) {
            throw new TableFileReadException(e);
        }
    }
}
