package msd.tablemanager.support.jpa;

import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import java.util.StringTokenizer;

/**
 * JpaUtilities.
 *
 * @author pkotula
 */
public final class JpaUtilities {
    private JpaUtilities() {

    }

    /**
     * Gets the path.
     *
     * @param <Y>           the generic type
     * @param attributeName the attribute name
     * @param root          the root
     * @return the path
     */
    public static <Y> Path<Y> getPath(String attributeName, Root<?> root) {
        Path<Y> retVal = null;

        if (attributeName != null && root != null) {
            Path<Y> path = null;
            StringTokenizer st = new StringTokenizer(attributeName, ".");
            String a;
            while (st.hasMoreElements()) {
                a = st.nextToken();
                if (path == null) {
                    path = root.get(a);
                } else {
                    path = path.get(a);
                }
            }
            retVal = path;
        }

        return retVal;
    }

}
