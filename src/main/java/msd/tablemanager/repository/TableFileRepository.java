package msd.tablemanager.repository;

import msd.tablemanager.domain.TableFile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Table file repository.
 *
 * @author pkotula
 */
@RestResource(exported = false)
@Repository
public interface TableFileRepository extends JpaRepository<TableFile, Long>, JpaSpecificationExecutor<TableFile> {

    /**
     * Find by name.
     *
     * @param name the name
     * @return the list
     */
    List<TableFile> findByName(String name);
}
