package msd.tablemanager.repository;

import msd.tablemanager.domain.TableFileRow;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Table file row repository.
 *
 * @author pkotula
 */
@RestResource(exported = false)
@Repository
public interface TableFileRowRepository extends JpaRepository<TableFileRow, Long>, JpaSpecificationExecutor<TableFileRow> {

    /**
     * Find by file name list.
     *
     * @param name the name
     * @return the list
     */
    @Query("select tfr from TableFileRow tfr join tfr.file tf where tf.name = :name")
    List<TableFileRow> findByFileName(String name);
}
