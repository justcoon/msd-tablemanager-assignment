package msd.tablemanager.repository;

import msd.tablemanager.domain.TableFileRow;
import msd.tablemanager.support.jpa.JpaUtilities;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.Assert;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

/**
 * Table file row specification.
 *
 * @param <T> the type parameter
 * @author pkotula
 */
public class TableFileRowSpecification<T extends TableFileRow> implements Specification<T> {
    /**
     * The Name.
     */
    protected String name;
    /**
     * The First row index.
     */
    protected Integer firstRowIndex;
    /**
     * The Last row index.
     */
    protected Integer lastRowIndex;

    /**
     * Instantiates a new Table file row specification.
     *
     * @param name          the name
     * @param firstRowIndex the first row index
     * @param lastRowIndex  the last row index
     */
    public TableFileRowSpecification(String name, Integer firstRowIndex, Integer lastRowIndex) {
        Assert.notNull(name,"Name is required");
        this.name = name;
        this.firstRowIndex = firstRowIndex;
        this.lastRowIndex = lastRowIndex;
    }

    @Override
    public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        List<Predicate> predicates = new ArrayList<>();
        predicates.add(cb.equal(JpaUtilities.getPath("file.name", root), name));

        if (firstRowIndex != null) {
            predicates.add(cb.greaterThanOrEqualTo(JpaUtilities.getPath("rowIndex", root), firstRowIndex));
        }
        if (lastRowIndex != null) {
            predicates.add(cb.lessThan(JpaUtilities.getPath("rowIndex", root), lastRowIndex));
        }

        return cb.and(predicates.toArray(new Predicate[predicates.size()]));
    }
}
