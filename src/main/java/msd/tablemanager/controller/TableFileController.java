package msd.tablemanager.controller;

import msd.tablemanager.Application;
import msd.tablemanager.domain.TableFile;
import msd.tablemanager.domain.TableFileRow;
import msd.tablemanager.service.TableFileService;
import msd.tablemanager.support.Utilities;
import msd.tablemanager.support.dto.XmlHeader;
import msd.tablemanager.support.dto.XmlRows;
import msd.tablemanager.support.reader.TableFileReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Optional;

/**
 * Table file controller.
 *
 * @author pkotula
 */
@RestController
@RequestMapping("/file")
public class TableFileController {
    private static final Logger LOG = LoggerFactory.getLogger(TableFileController.class);

    /**
     * The Table file service.
     */
    @Autowired
    TableFileService tableFileService;

    /**
     * The Table file reader.
     */
    @Autowired
    TableFileReader tableFileReader;

    /**
     * File upload
     *
     * @param file the file
     * @return the response entity
     * @throws Exception the exception
     */
    @PostMapping("/upload")
    public ResponseEntity fileUpload(@RequestParam("file") MultipartFile file, @RequestParam(value = "separator", required = false) Character separator) throws Exception {

        if (file.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }

        try {
            List<String[]> values = tableFileReader.readData(file.getInputStream(), separator == null ? Application.TABLE_COLUMN_SEPARATOR : separator);

            TableFile tableFile = tableFileService.createOrUpdateFile(file.getOriginalFilename(), values);

            return ResponseEntity.ok(tableFile.getName());
        } catch (Exception e) {
            LOG.error("File processing error", e);
            throw e;
        }
    }

    /**
     * Gets header.
     *
     * @param name the name - required
     * @return the header
     */
    @RequestMapping(path = "/header", method = RequestMethod.GET, produces = "application/xml")
    public ResponseEntity<XmlHeader> getHeader(@RequestParam String name) {
        Optional<TableFile> tableFile = tableFileService.getFile(name);

        return tableFile.map(tf -> ResponseEntity.ok(XmlHeader.instanceOf(tf.getHeader())))
                .orElse(ResponseEntity.status(HttpStatus.NOT_FOUND).build());
    }


    /**
     * Gets rows.
     *
     * @param name          the name - required
     * @param firstRowIndex the first row index
     * @param lastRowIndex  the last row index
     * @return the rows
     */
    @RequestMapping(path = "/rows", method = RequestMethod.GET, produces = "application/xml")
    public ResponseEntity<XmlRows> getRows(@RequestParam String name, @RequestParam(required = false) Integer firstRowIndex, @RequestParam(required = false) Integer lastRowIndex) {
        Optional<List<TableFileRow>> tableFileRows = tableFileService.getFileRows(name, firstRowIndex, lastRowIndex);

        return tableFileRows.map(tfr -> Utilities.getXmlRows(tfr))
                .map(rows -> ResponseEntity.ok(rows))
                .orElse(ResponseEntity.status(HttpStatus.NOT_FOUND).build());
    }
}
