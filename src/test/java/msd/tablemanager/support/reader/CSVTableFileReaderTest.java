package msd.tablemanager.support.reader;

import msd.tablemanager.Application;
import org.junit.Before;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;

import java.io.InputStream;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

/**
 * {@link CSVTableFileReader} tests
 *
 * @author pkotula
 */
public class CSVTableFileReaderTest {

    CSVTableFileReader csvTableFileReader;

    @Before
    public void init() {
        csvTableFileReader = new CSVTableFileReader();
    }

    @Test
    public void testCsvTest() throws Exception {

        //null inputStream test
        assertThatThrownBy(() -> csvTableFileReader.readData(null, Application.TABLE_COLUMN_SEPARATOR)).isInstanceOf(IllegalArgumentException.class);

        // test.csv test
        ClassPathResource resource = new ClassPathResource("test.csv");
        InputStream inputStream = resource.getInputStream();

        List<String[]> values = csvTableFileReader.readData(inputStream, Application.TABLE_COLUMN_SEPARATOR);
        assertThat(values).isNotEmpty().hasSize(5);

        inputStream.close();
    }
}
