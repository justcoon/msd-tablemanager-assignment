package msd.tablemanager.support;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

/**
 * {@link Utilities} tests
 *
 * @author pkotula
 */
public class UtilitiesTest {


    @Test
    public void getValuesTest() throws Exception {


        //null values test
        assertThatThrownBy(() -> Utilities.getValues(null, 3)).isInstanceOf(IllegalArgumentException.class);

        //values test

        String[] values = new String[]{"abc", "abcd", "xy"};

        List<String> outputValues = Utilities.getValues(values, 3);

        List<String> expectedValues = Arrays.asList("abc", "abc", "xy");

        assertThat(outputValues).isNotEmpty().isEqualTo(expectedValues);

    }
}
