package msd.tablemanager.service;

import msd.tablemanager.Application;
import msd.tablemanager.domain.TableFile;
import msd.tablemanager.domain.TableFileRow;
import msd.tablemanager.support.reader.TableFileReader;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.context.ConfigFileApplicationContextInitializer;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import java.io.InputStream;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * {@link TableFileService} tests
 *
 * @author pkotula
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Transactional
public class TableFileServiceTest {

    @EntityScan(basePackageClasses = Application.class)
    @ComponentScan(basePackageClasses = Application.class)
    @EnableJpaRepositories(basePackageClasses = Application.class)
    @EnableTransactionManagement
    @EnableAutoConfiguration
    @Configuration
    @ContextConfiguration(
            initializers = ConfigFileApplicationContextInitializer.class)
    public static class Config {
    }

    @Autowired
    TableFileService tableFileService;

    @Autowired
    TableFileReader tableFileReader;

    private List<String[]> getData(String fileName) throws Exception {
        ClassPathResource resource = new ClassPathResource(fileName);
        InputStream inputStream = resource.getInputStream();
        List<String[]> values = tableFileReader.readData(inputStream, Application.TABLE_COLUMN_SEPARATOR);
        inputStream.close();
        return values;
    }

    @Test
    public void createAndUpdateFileTest() throws Exception {

        //create file
        List<String[]> values = getData("test.csv");
        assertThat(values).isNotEmpty();
        TableFile tableFile = tableFileService.createOrUpdateFile("test.csv", values);

        assertThat(tableFile).isNotNull();
        assertThat(tableFile.getName()).isEqualTo("test.csv");
        assertThat(tableFile.getHeader()).isNotEmpty();
        assertThat(tableFile.getRows()).isNotEmpty().hasSize(4);

        //update file
        List<String[]> updatedValues = values.subList(0, 3);
        TableFile tableFileUpdated = tableFileService.createOrUpdateFile("test.csv", updatedValues);

        assertThat(tableFileUpdated).isNotNull();
        assertThat(tableFileUpdated.getId()).isEqualTo(tableFile.getId());
        assertThat(tableFileUpdated.getName()).isEqualTo("test.csv");
        assertThat(tableFileUpdated.getHeader()).isNotEmpty();
        assertThat(tableFileUpdated.getRows()).isNotEmpty().hasSize(2);
    }

    @Test
    public void getHeaderTest() throws Exception {

        //create file
        List<String[]> values = getData("test.csv");
        assertThat(values).isNotEmpty();
        TableFile tableFile = tableFileService.createOrUpdateFile("test_header.csv", values);


        assertThat(tableFile).isNotNull();
        assertThat(tableFile.getName()).isEqualTo("test_header.csv");
        assertThat(tableFile.getHeader()).isNotEmpty();
        assertThat(tableFile.getRows()).isNotEmpty().hasSize(4);

        //test - headers
        Optional<TableFile> tableFileOptional = tableFileService.getFile("test_header.csv");

        assertThat(tableFileOptional).isNotNull().isNotEmpty();

        assertThat(tableFileOptional.get().getHeader()).isNotEmpty().containsExactly("email", "username", "description");

        //test - file not found
        Optional<TableFile> tableFileNotFoundOptional = tableFileService.getFile("test_header_not_found.csv");

        assertThat(tableFileNotFoundOptional).isNotNull().isEmpty();
    }

    @Test
    public void getRowsTest() throws Exception {

        //create file
        List<String[]> values = getData("test.csv");
        assertThat(values).isNotEmpty();
        TableFile tableFile = tableFileService.createOrUpdateFile("test_rows.csv", values);

        assertThat(tableFile).isNotNull();
        assertThat(tableFile.getName()).isEqualTo("test_rows.csv");
        assertThat(tableFile.getHeader()).isNotEmpty();
        assertThat(tableFile.getRows()).isNotEmpty().hasSize(4);

        // test  - all rows
        Optional<List<TableFileRow>> tableFileRowsOptional = tableFileService.getFileRows("test_rows.csv", null, null);

        assertThat(tableFileRowsOptional).isNotNull().isNotEmpty();
        assertThat(tableFileRowsOptional.get()).isNotEmpty().hasSize(4);
        assertThat(tableFileRowsOptional.get().stream().map(tr -> tr.getRowIndex()).collect(Collectors.toList()))
                .isEqualTo(IntStream.range(0, 4).boxed().collect(Collectors.toList()));

        //test - rows from 1 until 3
        tableFileRowsOptional = tableFileService.getFileRows("test_rows.csv", 1, 3);

        assertThat(tableFileRowsOptional).isNotNull().isNotEmpty();
        assertThat(tableFileRowsOptional.get()).isNotEmpty().hasSize(2);
        assertThat(tableFileRowsOptional.get().stream().map(tr -> tr.getRowIndex()).collect(Collectors.toList()))
                .isEqualTo(IntStream.range(1, 3).boxed().collect(Collectors.toList()));


        //test - rows until 3
        tableFileRowsOptional = tableFileService.getFileRows("test_rows.csv", null, 3);

        assertThat(tableFileRowsOptional).isNotNull().isNotEmpty();
        assertThat(tableFileRowsOptional.get()).isNotEmpty().hasSize(3);
        assertThat(tableFileRowsOptional.get().stream().map(tr -> tr.getRowIndex()).collect(Collectors.toList()))
                .isEqualTo(IntStream.range(0, 3).boxed().collect(Collectors.toList()));

        //test - rows until 3
        tableFileRowsOptional = tableFileService.getFileRows("test_rows.csv", 1, null);

        assertThat(tableFileRowsOptional).isNotNull().isNotEmpty();
        assertThat(tableFileRowsOptional.get()).isNotEmpty().hasSize(3);
        assertThat(tableFileRowsOptional.get().stream().map(tr -> tr.getRowIndex()).collect(Collectors.toList()))
                .isEqualTo(IntStream.range(1, 4).boxed().collect(Collectors.toList()));

        //test - file not found
        Optional<List<TableFileRow>> tableFileRowsNotFoundOptional = tableFileService.getFileRows("test_rows_not_found.csv", null, null);

        assertThat(tableFileRowsNotFoundOptional).isNotNull().isEmpty();
    }
}