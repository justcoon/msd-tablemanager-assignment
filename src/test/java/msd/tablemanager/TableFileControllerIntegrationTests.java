package msd.tablemanager;

import msd.tablemanager.domain.TableFile;
import msd.tablemanager.service.TableFileService;
import msd.tablemanager.support.dto.XmlHeader;
import msd.tablemanager.support.dto.XmlRows;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.context.ConfigFileApplicationContextInitializer;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.Arrays;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * {@link msd.tablemanager.controller.TableFileController} tests
 *
 * @author pkotula
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TableFileControllerIntegrationTests {

    @EntityScan(basePackageClasses = Application.class)
    @ComponentScan(basePackageClasses = Application.class)
    @EnableJpaRepositories(basePackageClasses = Application.class)
    @EnableTransactionManagement
    @EnableAutoConfiguration
    @Configuration
    @ContextConfiguration(
            initializers = ConfigFileApplicationContextInitializer.class)
    public static class Config {
    }

    @Autowired
    TestRestTemplate restTemplate;

    @Autowired
    TableFileService tableFileService;


    @Test
    public void fileTests() throws Exception {
        String testFileName = "test.csv";

        //upload file test
        ClassPathResource resource = new ClassPathResource(testFileName);

        MultiValueMap<String, Object> map = new LinkedMultiValueMap<String, Object>();
        map.add("file", resource);
        ResponseEntity<String> response = this.restTemplate.postForEntity("/file/upload", map, String.class);

        assertThat(response.getStatusCode()).isEqualByComparingTo(HttpStatus.OK);
        assertThat(response.getBody()).isEqualTo(testFileName);

        Optional<TableFile> tableFileOptional = tableFileService.getFile(testFileName);
        assertThat(tableFileOptional).isNotNull().isNotEmpty();

        //get header
        ResponseEntity<XmlHeader> getHeaderResponse = this.restTemplate.getForEntity("/file/header?name=" + testFileName, XmlHeader.class);

        assertThat(getHeaderResponse.getStatusCode()).isEqualByComparingTo(HttpStatus.OK);
        assertThat(getHeaderResponse.getBody()).isEqualTo(XmlHeader.instanceOf(Arrays.asList("email", "username", "description")));

        //get rows
        ResponseEntity<XmlRows> getRowsResponse = this.restTemplate.getForEntity("/file/rows?name=" + testFileName, XmlRows.class);

        assertThat(getRowsResponse.getStatusCode()).isEqualByComparingTo(HttpStatus.OK);
        assertThat(getRowsResponse.getBody()).isInstanceOf(XmlRows.class);
        assertThat(getRowsResponse.getBody().getRows()).isNotEmpty().hasSize(4);

        getRowsResponse = this.restTemplate.getForEntity("/file/rows?name=" + testFileName + "&firstRowIndex=0&lastRowIndex=1", XmlRows.class);

        assertThat(getRowsResponse.getStatusCode()).isEqualByComparingTo(HttpStatus.OK);
        assertThat(getRowsResponse.getBody()).isInstanceOf(XmlRows.class);
        assertThat(getRowsResponse.getBody().getRows()).isNotEmpty().hasSize(1);

        getRowsResponse = this.restTemplate.getForEntity("/file/rows?name=" + testFileName + "&lastRowIndex=1", XmlRows.class);

        assertThat(getRowsResponse.getStatusCode()).isEqualByComparingTo(HttpStatus.OK);
        assertThat(getRowsResponse.getBody()).isInstanceOf(XmlRows.class);
        assertThat(getRowsResponse.getBody().getRows()).isNotEmpty().hasSize(1);


        //get header - not found test
        getHeaderResponse = this.restTemplate.getForEntity("/file/header?name=test_not_found.csv", XmlHeader.class, map);

        assertThat(getHeaderResponse.getStatusCode()).isEqualByComparingTo(HttpStatus.NOT_FOUND);

        //get rows - not found test
        getRowsResponse = this.restTemplate.getForEntity("/file/rows?name=test_not_found.csv", XmlRows.class, map);

        assertThat(getRowsResponse.getStatusCode()).isEqualByComparingTo(HttpStatus.NOT_FOUND);
    }

}