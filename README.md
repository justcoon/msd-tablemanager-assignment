#MSD Table Manager Application

application is using _spring-boot_ framework

##Configuration

application configuration: _application.properties_

data store H2 database with location specified by:

```
spring.datasource.url=jdbc\:h2\:/tmp/msd_tablemanager;MV_STORE=FALSE;MVCC=FALSE
```

##Build application

build application with gradle, from project root folder execute

unix/linux
```
./gradlew build
```

windows
```
./gradlew.bat build
```



##Run application

run application with gradle, from project root folder execute

unix/linux
```
./gradlew bootRun
```

windows
```
./gradlew.bat bootRun
```


##Upload file

conditions:
* default separator is ',', separator (one character) can be changed by request parameter (see following example);
* max. value length for header value and row value is 2000 (longer values are trimmed);
* system does not validate file type (if file type is not parsable as csv - system throws error);


upload file _test.csv_ (default separator is ',')

```
curl -i -X POST -H "Content-Type: multipart/form-data" -F "file=@test.csv" http://localhost:8080/file/upload

```

response:
* response body - file name
* file processing error - HTTP status 500 (internal server error)


upload file _test.csv_ with column separator ';' (semicolon ASCII code %3B)

```
curl -i -X POST -H "Content-Type: multipart/form-data" -F "file=@test.csv" http://localhost:8080/file/upload?separator=%3B
```

##Get header

get header for file _test.csv_

```
curl -v 'http://localhost:8080/file/header?name=test.csv'
```

response body example (if file exists)

```
<header>
 <value>email</value>
 <value>username</value>
 <value>description</value>
</header>
```

if file not exists - HTTP status 404 (not found)

##Get all rows

get all rows for file _test.csv_

```
curl -v 'http://localhost:8080/file/rows?name=test.csv'
```

response body example (if file exists) 
```
<rows>
 <row>
  <value>aaa@aaa.com</value>
  <value>aaa</value>
  <value>neww description description</value>
 </row>
 <row>
  <value>xxx@xxx.com</value>
  <value>xxx</value>
  <value>neww xxx description description</value>
 </row>
 <row>
  <value>ccc@ccc.com</value>
  <value>ccc</value>
  <value>neww ccc description description</value>
 </row>
 <row>
  <value>bbb@bbb.com</value>
  <value>bbb</value>
  <value>neww description description</value>
 </row>
</rows>
```
if file not exists - HTTP status 404 (not found)


##Get rows by index

get rows for file _test.csv_ from index 0 to index 1

```
curl -v 'http://localhost:8080/file/rows?name=test.csv&firstRowIndex=0&lastRowIndex=1'
```

response body example (if file exists)

```
<rows>
 <row>
  <value>aaa@aaa.com</value>
  <value>aaa</value>
  <value>neww description description</value>
 </row>
</rows>
```

if file not exists - HTTP status 404 (not found)